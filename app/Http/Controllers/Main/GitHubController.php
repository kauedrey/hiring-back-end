<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Services\Main\GitHubService;

class GitHubController extends Controller
{

    /**
     * @var GitHubService
     */
    private $gitHubService;

    /**
     * @param GitHubService $gitHubService
     */
    public function __construct(GitHubService $gitHubService)
    {
        $this->gitHubService = $gitHubService;
    }
    
    public function getUser($username)
    {
        try {
            return $getUser = $this->gitHubService->getUser($username);
        } catch (\Exception $err) {
            return response()->json([
                'error'   => 'Usuário não encontrado',
            ], 404);
        }
    }
    
    public function getUserRepositories($username)
    {
        try {
            return $this->gitHubService->getUserRepositories($username);
        } catch (\Exception $err) {
            return response()->json([
                'error'   => 'Usuário não encontrado',
            ], 404);
        }
    }
}
