<?php

namespace App\Http\Resources\Main;

use Illuminate\Http\Resources\Json\Resource;

class GitHubUserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'login' => $this['login'],
            'name' => $this['name'],
            'avatar_url' => $this['avatar_url'],
            'html_url' => $this['html_url']
        ];
    }
}
