<?php

namespace App\Http\Resources\Main;

use Illuminate\Http\Resources\Json\Resource;

class GitHubUserRepositoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this['id'],
            'name' => $this['name'],
            'description' => $this['description'],
            'html_url' => $this['html_url']
        ];
    }
}