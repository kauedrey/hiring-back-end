<?php

namespace Services\Main;

use GuzzleHttp\Client;
use App\Http\Resources\Main\GitHubUserResource;
use App\Http\Resources\Main\GitHubUserRepositoryResource;

class GitHubService
{
    /**
     * @param mixed $username
     * @return mixed
     */
    public function getUser($username)
    {
        $client = new Client();
        $res = $client->request('GET', 'https://api.github.com/users/' . $username);
        $res->getStatusCode();
        $content = json_decode($res->getBody(), true);
        return new GitHubUserResource(collect($content));
    }

    /**
     * @param mixed $username
     * @return array
     */
    public function getUserRepositories($username)
    {
        $client = new Client();
        $res = $client->request('GET', 'https://api.github.com/users/' . $username . '/repos');
        $res->getStatusCode();
        $content = json_decode($res->getBody(), true);
        return GitHubUserRepositoryResource::collection(collect($content));
    }
}
