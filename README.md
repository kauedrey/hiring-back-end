# Hiring Creditoo

Follow the steps below to run the test.


## Requirements

- https://docker.com

## Install

In the project root run:
```bash
docker-compose run app bash
```
Inside the container run:
```bash
composer install
```


To exit the container run:
```bash
exit
```

To start services run:
```bash
docker-compose up
```


## Routes

**Get user profile**

Request

GET localhost/api/users/{username}

 
**Get user repositories**

Request

GET localhost/api/users/{username}/repos

## Tests

In the project root run:
```bash
docker-compose run app bash
```

Inside the container run:
```bash
vendor/bin/phpunit
