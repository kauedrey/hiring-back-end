<?php

namespace Tests\Feature\Main;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserGitHubTest extends TestCase
{
    
    public function test_get_user_by_username()
    {
        $response = $this->json('GET', '/api/users/kauedrey');
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'login',
                'name',
                'avatar_url',
                'html_url'
            ]);
    }
    
    public function test_get_user_repositories_by_username()
    {
        $response = $this->json('GET', '/api/users/kauedrey/repos');
        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'description',
                    'html_url'
                ]
            ]);
    }
}
